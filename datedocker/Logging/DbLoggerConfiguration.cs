﻿namespace SqlEfApi.Logging
{
    public class DbLoggerConfiguration
    {
        public string DbConnectionString { get; set; }
    }
}
