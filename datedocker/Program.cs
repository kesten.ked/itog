using Microsoft.EntityFrameworkCore;
using SqlEfApi.Data;
using SqlEfApi.Logging;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Logging.ClearProviders()
    .AddDbLogger(configure => { });

// ��� Docker

builder.Services.AddDbContext<EFContext>(opt =>
{
    var config = builder.Configuration;
    // ��� �������� � ����������
    var server = config["DBserver"] ?? "51.250.70.51";
    // ��� �������� �� Windows
    // var server = config["DbServer"] ?? "localhost";
    var port = config["DbPort"] ?? "5432";
    var dbName = config["DBname"] ?? "DB";
    var user = config["DbUser"] ?? "postgres";
    var password = config["DbPassword"] ?? "PassCd12345678";

    var conectionString = $"Host={server};Port={port};Database={dbName};Username={user};Password={password}";

    opt.UseNpgsql(conectionString);
});


/*
builder.Services.AddDbContext<EFContext>(opt =>
{
    var config = builder.Configuration.GetSection("ConnectionStrings")
        .Get<ConnectionConfiguration>();
    opt.UseSqlServer(config.SqlEfApiConnection);
});
*/

// ������������ ���� sql
// builder.PopulateDB();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
