variable "terraform_token"{
  type = string
}

variable "terraform_cloud_id"{
  type = string
}

variable "terraform_folder_id"{
  type = string
}

variable "AWS_ACCESS_KEY_ID"{
  type = string
}

variable "AWS_SECRET_KEY_ID"{
  type = string
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "s3lll"
    region     = "ru-central1"
    key        = "itogk8s.tfstate"
    access_key = var.AWS_ACCESS_KEY_ID
    secret_key = var.AWS_SECRET_KEY_ID

    skip_region_validation      = true
    skip_credentials_validation = true
  }

  required_version = ">= 0.74.0"
}

provider "yandex" {
  token     = var.terraform_token
  cloud_id  = var.terraform_cloud_id
  folder_id = var.terraform_folder_id
}
